devops-recruitment-tasks
========================

Please see the [wiki](https://gitlab.com/recruitment-tasks/devops/wikis/home)


Terraform code modified to fit requirements of the recruitment tasks,  
Please add your profile to the AWS provider in terraform.tf


