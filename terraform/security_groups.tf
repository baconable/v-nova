resource "aws_security_group" "packer_build" {
  name        = "${var.env}-packer-build"
  description = "Security Group for Packer Builds"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = "${var.env}-packer-build"
    },
    var.tags,
  )
}

resource "aws_security_group" "webserver_security_group" {
  name        = "${var.env}-webserver-sg"
  description = "${var.env} Security Group for Web Server"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${aws_instance.bastion.private_ip}/32"]
  }

  tags = merge(
    {
      "Name" = "${var.env}-webserver-access"
    },
    var.tags,
  )
}

resource "aws_security_group" "bastion_security_group" {
  name        = "${var.env}-bastion-server"
  description = "${var.env} Security Group for bastion server"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = "${var.env}-bastion-access"
    },
    var.tags,
  )
}
