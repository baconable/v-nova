variable "access_key" {
  description = "aws access key,  you need to specify the correct key for the account being used"
  type        = string
}

variable "secret_key" {
  description = "aws secret key,  you need to specify the correct key for the account being used"
  type        = string
}

variable "env" {
  description = "environment tag to be used in tags and naming"
  type        = string
  default     = "dev"
}

variable "region" {
  description = "Default region to be used if no other is specified"
  type        = string
  default     = "eu-west-1"
}

variable "vpc_cidr" {
  description = "Default vpc cidr to be used if no other is specified"
  type        = string
  default     = "10.9.0.0/16"
}

variable "private_subnets" {
  description = "private subnets to be used when provisioning resources"
  type        = list
  default     = ["10.9.0.0/24", "10.9.1.0/24"]
}

variable "public_subnets" {
  description = "public subnets to be used when provisioning resources"
  type        = list
  default     = ["10.9.10.0/24", "10.9.11.0/24"]
}

variable "database_subnets" {
  description = "database subnets to be used when provisioning resources"
  type        = list
  default     = ["10.9.20.0/24", "10.9.21.0/24"]
}

variable "sshpubkey_file" {
  description = "ssh key file to be used when provisioning ec2 instances"
  type        = string
}

variable "instance_type" {
  description = "default instance type to be used if no other is specified"
  type        = string
  default     = "t2.micro"
}

variable "tags" {
  description = "default tags to use for all resources"
  type        = map
  default = {
    "env"    = "dev"
    "author" = "baconable - simon@rashers.net"
  }
}

