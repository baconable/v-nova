resource "aws_instance" "bastion" {
  ami             = data.aws_ami.amazon_linux.id
  instance_type   = var.instance_type
  key_name        = var.sshpubkey_file
  security_groups = [aws_security_group.bastion_security_group.id]
  subnet_id       = module.vpc.public_subnets[0]

  tags = merge(
    {
      "Name" = "${var.env}-bastion"
    },
    var.tags,
  )

}

resource "aws_eip" "bastion-server" {
  instance = aws_instance.bastion.id
  vpc      = true
}

resource "aws_instance" "web" {
  ami             = data.aws_ami.amazon_linux.id
  instance_type   = var.instance_type
  key_name        = var.sshpubkey_file
  security_groups = [aws_security_group.webserver_security_group.id]
  subnet_id       = module.vpc.public_subnets[0]
  tags = merge(
    {
      "Name" = "${var.env}-webserver"
    },
    var.tags,
  )
}

resource "aws_eip" "webserver" {
  instance = aws_instance.web.id
  vpc      = true
}
