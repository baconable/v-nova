resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${data.aws_caller_identity.current.account_id}-${var.region}-${var.env}-s3transfer"
  acl    = "private"

  tags = merge(
    {
      "Name" = "${var.env}-s3transfers"
    },
    var.tags,
  )
}
